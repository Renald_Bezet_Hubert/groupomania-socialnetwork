/**
 * require Express, Router method & controller Posts
 * export module
 */

// REQUIRED
const express = require('express')
const postCtrl = require('../controllers/post')
const auth = require('../middlewares/auth')
const multer = require('../middlewares/multer')
const router = express.Router()

// ROUTES
router.get('/', auth, postCtrl.getAllPosts)
router.post('/', auth, multer, postCtrl.createPost)
router.put('/:id', auth, multer, postCtrl.modifyPost)
router.delete('/:id', auth, postCtrl.deletePost)

// EXPORT
module.exports = router
