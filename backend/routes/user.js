/**
 * require Express, Router method & controller User
 * use router to define post to database( signup/login)
 * export module
 */

// REQUIRED
const express = require('express')
const router = express.Router()
const multer = require('../middlewares/multer')
const userCtrl = require('../controllers/user')
const auth = require('../middlewares/auth')

// ROUTES
router.post('/signup', userCtrl.signup)
router.post('/login', userCtrl.login)
router.get('/mycount/:id', auth, userCtrl.getOneUser)
router.put('/mycount/:id', multer, auth, userCtrl.updateUser)
router.delete('/mycount/:id', auth, userCtrl.deleteUser)

// EXPORT
module.exports = router
