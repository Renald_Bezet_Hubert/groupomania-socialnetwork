-- 2 Tables to manages Database : users, posts
-- user have an Id and Identity,  want to  post something or react with a comment 
-- post belongs to the user and could have many comments, have a title, content and a date and date
-- comments belongs to a post and the user, have a content and a date

-- USER TABLE
DROP TABLE IF EXISTS `Users`;
CREATE TABLE `users` (
	`id` mediumint UNSIGNED NOT NULL AUTO_INCREMENT,
	`firstname` varchar(25) NOT NULL,
	`lastname` varchar(25) NOT NULL,
	`email` varchar(50) NOT NULL UNIQUE,
	`password` varchar(255) NOT NULL,
	`avatar` varchar(255) NOT NULL DEFAULT '../../frontend/public/images/avatar.jpg',
	`admin` tinyint(1) NOT NULL DEFAULT '0',
	PRIMARY KEY (`userId`)
);
-- AUTO_INCREMENT define the start of incrementation
ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8_general_ci; 
--

--POSTS TABLE
DROP TABLE IF EXISTS `Posts`;
CREATE TABLE `posts` (
	`id` mediumint UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` mediumint NOT NULL,
	`content` text NOT NULL,
	`media` text DEFAULT NULL,
	PRIMARY KEY (`id`)
	KEY `fk_userId` (`userId`),
	CONSTRAINT `fk_userId` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);
ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8_general_ci; 
--

-- FOREIGN KEYS

ALTER TABLE `POSTS` ADD CONSTRAINT `POSTS_fk0` FOREIGN KEY (`user_id`) REFERENCES `USERS`(`user_id`);
--

-- WRITING
--LOCK TABLES `Users` WRITE;
--INSERT INTO `Users` VALUES(23, )
--UNLOCK TABLES;

--LOCK TABLES `Posts` WRITE;
--
--UNLOCK TABLES;

