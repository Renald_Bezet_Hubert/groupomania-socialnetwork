/**
 * Connect DB and Models to manage data.
 * mysql
 */

// REQUIRED
require('dotenv').config()
const mysql = require('mysql')

// Configure DB Connection
const connectData = mysql.createConnection({
  host: process.env.HOST,
  dialect: 'mysql',
  database: process.env.DATABASE,
  user: process.env.ADMIN,
  password: process.env.ADMIN_PASSWORD
})

// TEST CONNECT

connectData.connect((error) => {
  if (error) throw error
  console.log('Connected')
})

module.exports = connectData
