/**
 * require on
 * framework Express
 * packages :
 * DOTENV | MYSQL | CORS | BODY-PARSER | PATH | HELMET
 */
// require('dotenv').config()
// require framework, packages and routes
const express = require('express')
const cors = require('cors')
const path = require('path')
const bodyParser = require('body-parser')
const helmet = require('helmet')
const routeUser = require('./routes/user')
const routePost = require('./routes/post')
// const routeComment = require('./routes/comment')
require('./sql/connect_db')
// use Express
const app = express()

// App use..

// CORS part
app.use(cors())

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization')
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS')
  next()
})

// Protect API: header HTTP
app.use(helmet())

// Manage JSON: body request in JS object
app.use(bodyParser.json())

// Manage Images
app.use('/images', express.static(path.join(__dirname, 'images')))

// Routes
app.use('/post', routePost)
app.use('/user', routeUser)

// export for server
module.exports = app
