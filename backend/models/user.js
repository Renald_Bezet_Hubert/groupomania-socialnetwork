/**
 * Create model USER
 * Naming packages using with package'name
 */
class User {
  constructor (user) {
    this.userId = user.user_Id // PRIMARY KEY
    this.firstname = user.firstname
    this.lastname = user.lastname
    this.email = user.email
    this.password = user.password
    this.avatar = user.avatar
    this.admin = user.admin
  }
}

module.exports = User
