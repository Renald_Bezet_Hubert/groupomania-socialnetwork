/**
 * Model constructor : POSTS
 */

class Post {
  constructor (post) {
    // this.postId = post.postId //PRIMARY KEY
    this.title = post.title
    this.content = post.content
    this.date = post.date
    this.totalComment = post.totalComment
    this.userId = post.userId // FOREIGN KEY
  }
}

module.exports = Post
