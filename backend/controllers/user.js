/**
 * USER
 */

// REQUIRED
const bcrypt = require('bcrypt')
const jsonWebToken = require('jsonwebtoken')
const validator = require('validator')
const fs = require('fs')
const User = require('../models/user')
const connectDatabase = require('../sql/connect_db')
require('dotenv').config()

// SIGNUP : validate email and password, then mask email( todo), hash password and save
exports.signup = async (req, res) => {
  if (validator.isEmail(req.body.email) === false || validator.isStrongPassword(req.body.password) === false) {
    return res.status(400).json({ message: 'L\'email  et le mot de passe ne sont pas au bon format. le Mot de passe doit contenir : 8 caractères et au moins une minuscule, une Majuscule, un chiffre et un symbol' })
  } else if (validator.isEmail(req.body.email) && validator.isStrongPassword(req.body.password)) {
    if (req.body.email === process.env.ADMIN_USER && req.body.password === process.env.ADMIN_PASSWORD) {
      const hash = await bcrypt.hash(req.body.password, 10)
      const user = new User({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        password: hash,
        admin: 1
      })
      connectDatabase.query('INSERT INTO users(firstname, lastname, email, password, admin) VALUES(?, ?, ?, ?, ?)', // new user (admin)
        [user.firstname, user.lastname, user.email, user.password, user.admin])
      return res.status(201).json({ user, message: 'Compte créé !' })
    } else {
      const hash = await bcrypt.hash(req.body.password, 10)

      const user = new User({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        password: hash
      })
      connectDatabase.query('INSERT INTO users(firstname, lastname, email, password) VALUES(?, ?, ?, ?)',
        [user.firstname, user.lastname, user.email, user.password])
      return res.status(201).json({ user, message: 'Votre compte a été créé !' })
    }
  }
}

// LOGIN : 1.check email, then password( false = error : give a token)
exports.login = (req, res) => {
  try {
    connectDatabase.query('SELECT * FROM users WHERE email = ?', req.body.email, (err, row) => {
      if (err || row.length === 0) {
        return res.status(401).json({ message: 'Votre email ne correspond pas ou vous n\'êtes pas encore inscrit(e).' })
      } else {
        const user = row[0]
        console.log(row, user, user.password)
        bcrypt.compare(req.body.password, user.password)
          .then(data => {
            if (!data) {
              return res.status(401).json({ message: 'Votre mot de passe est incorrect.' })
            } else {
              const token = jsonWebToken.sign(
                { userId: user.user_id },
                'RANDOM_TOKEN_SECRET',
                { expiresIn: '12h' }
              )
              return res.status(200).json({
                userId: user.user_id,
                token: token,
                message: 'Vous êtes connecté(e)'
              })
            }
          })
      }
    })
  } catch (error) {
    return res.status(500).json({ message: error.message })
  }
}

// READ MY COUNT : check if count exist : if exist => ok
exports.mycount = (req, res) => {
  try {
    connectDatabase.query('SELECT * FROM users WHERE user_id = ?', req.query.userId, (err, row) => {
      if (err || row.length === 0) { 
        res.status(401).json({ message: 'Utilisateur non trouvé !' })
      } else {
        const user = row[0]
        res.status(200).json(user) 
      }
    })
  } catch (error) {
    res.status(500).json({ error })
  }
}

// UPDATE MY COUNT: use connectDatabase, check error || not result => no right to modify, user finded => rigth to modify
exports.updateUser = (req, res) => {
  try {
    connectDatabase.query('SELECT * FROM users WHERE userId = ?', req.query.userId, (err, row) => {
      if (err || row.length === 0) {
        return res.status(401).json({ message: 'Impossible de modifier le profil !' })
      } else {
        const user = row[0]
        if (req.file) {
          const path = user.avatar
          const filename = path.split('/')[1]
          if (filename !== 'avatar-default.png') {
            fs.unlink(`images/${filename}`, (err) => {
              if (err) throw err
            })
          }
        }
        // create profil with new avatar: modify profil
        const updateUser = req.file
          ? {
              firstname: req.body.firstname,
              lastname: req.body.lastname,
              email: req.body.email,
              avatar: `images/${req.file.filename}`
            }
          : {
              firstname: req.body.firstname,
              lastname: req.body.lastname,
              email: req.body.email
            }
        // if updating, then if firstname, lastname, avatar or just one
        let query = 'UPDATE users SET '
        let update = false
        if (updateUser.email !== user.email) {
          query += `email = '${updateUser.email}'`
          update = true
        }
        if (updateUser.firstname !== user.firstname) {
          if (update) {
            query += ', '
          }
          query += `firstname = '${updateUser.firstname}'`
          update = true
        }
        if (updateUser.lastname !== user.lastname) {
          if (update) {
            query += ', '
          }
          query += `lastname = '${updateUser.lastname}'`
          update = true
        }
        if (updateUser.avatar != null) {
          if (update) {
            query += ', '
          }
          query += `avatar = '${updateUser.avatar}'`
          update = true
        }
        // else= not updating
        if (update) {
          connectDatabase.query(query + 'WHERE userId = ?', req.query.userId)
          return res.status(200).json({ firstname: updateUser.firstname, lastname: updateUser.lastname, email: updateUser.email, avatar: updateUser.avatar ? updateUser.avatar : user.avatar, message: 'Profil modifié !' })
        } else {
          return res.status(204).json()
        }
      }
    })
  } catch (error) {
    return res.status(500).json({ message: error.message })
  }
}

// DELETE: check in DB ( error || user finded => search coms: delete all)
exports.deleteUser = (req, res) => {
  try {
    connectDatabase.query('SELECT * FROM users WHERE id= ?;', [req.params.id], (err, selectRows, selectFields) => {
      if (err) return res.status(400).json({ err })
      if (selectRows[0].avatar != null) {
        const filename = selectRows[0].avatar.split('/images/')[1]
        fs.unlink(`images/${filename}`, () => {
          connectDatabase.query('DELETE FROM users WHERE id= ?;', [req.params.id], (deleteErr, deleteRows, deleteFields) => {
            if (deleteErr) return res.status(400).json({ deleteErr })
            res.status(200).json({ message: 'le compte a été effacé.' })
          })
        })
      } else {
        connectDatabase.execute('DELETE FROM users WHERE id= ?;', [req.params.id], (deleteErr, deleteRows, deleteFields) => {
          if (deleteErr) return res.status(400).json({ deleteErr })
          res.status(200).json({ message: 'le compte est détruit.' })
        })
      };
    })
  } catch (error) {
    return res.status(500).json({ message: error.message })
  }
}
// GET ONE user : search user by id, check error || get user
exports.getOneUser = (req, res) => {
  try {
    connectDataectDatabase.query('SELECT * FROM users WHERE userId = ?', req.query.userId, (err, row) => { // vérification de la présence de l'utilisateur dans la bdd
      if (err || row.length === 0) {
        return res.status(401).json({ message: 'Veuillez vérifier vos informations!' })
      } else {
        const user = row[0]
        return res.status(200).json(user)
      }
    })
  } catch (error) {
    return res.status(500).json({ error })
  }
}

//  GET ALL POSTS: get Token(header auth) & verify, if ok = > search all posts => if post finded => get all posts
exports.getAllUserPosts = (req, res) => {
  try {
    const token = req.headers.authorization.split(' ')[1]
    const decodedToken = jsonWebToken.verify(token, 'RANDOM_TOKEN_SECRET')
    const userId = decodedToken.userId
    connectDataectDatabase.query('SELECT * FROM posts WHERE userId = ?', userId, (err, row) => {
      if (err || row.length === 0) {
        return res.status(401).json({ message: 'Articles non trouvés !' })
      } else {
        const post = row
        return res.status(200).json(post)
      }
    })
  } catch (error) {
    return res.status(500).json({ message: error.message })
  }
}
