/**
 * POST Controller
 */

// REQUIRED
// const jsonWebToken = require('jsonwebtoken')
// const Post = require('../models/Post')
const connectData = require('../sql/connect_db')
const fs = require('fs')

// POST A POST- check auth & token, search in DB => err : create new Post
exports.createPost = (req, res, next) => {
  console.log(req.body.post)
  const post = req.file
    ? {
        ...JSON.parse(req.body.post),
        mediaPost: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
      }
    : {
        ...req.body.post,
        postMedia: null
      }
  connectData.execute('INSERT INTO Posts VALUES (NULL, ?, ?, ?);', [post.user_id, post.content, post.media], (err, rows, fields) => {
    if (err) return res.status(400).json({ err })
    res.status(201).json({ message: 'Votre fichier est téléchargé' })
  })
}

// // MODIFY
exports.modifyPost = (req, res, next) => {
  const post = req.file
    ? {
        ...JSON.parse(req.body.post),
        postMedia: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
      }
    : {
        ...req.body.post,
        postMedia: null
      }
  connectData.execute('SELECT * FROM Posts WHERE id= ?;', [req.params.id], (selectErr, selectRows, selectFields) => {
    if (selectErr) return res.status(400).json({ selectErr })
    if (req.file = true && selectRows[0].media != null) {
      const filename = selectRows[0].postMedia.split('/images/')[1]
      fs.unlink(`images/${filename}`, () => {
        connectData.execute('UPDATE Posts SET content= ?, media= ? WHERE id= ?;', [post.content, post.media, req.params.id], (updateErr, updateRows, updateFields) => {
          if (updateErr) return res.status(400).json({ updateErr })
          res.status(200).json({ message: 'Votre message a été modifié. Le fichier a été effacé' })
        })
      })
    } else {
      connectData.execute('UPDATE Posts SET content= ?, media= ? WHERE id= ?;', [post.content, post.media, req.params.id], (updateErr, updateRows, updateFields) => {
        if (updateErr) return res.status(400).json({ updateErr })
        res.status(200).json({ message: 'Votre message a été modifié.' })
      })
    }
  })
}
// DELETE POST - DELETE
exports.deletePost = (req, res, next) => {
  connectData.execute('SELECT * FROM Posts WHERE id= ?;', [req.params.id], (selectErr, selectRows, selectFields) => {
    if (selectErr) return res.status(400).json({ selectErr })
    if (selectRows[0].media != null) {
      const filename = selectRows[0].media.split('/images/')[1]
      fs.unlink(`images/${filename}`, () => {
        connectData.execute('DELETE FROM Posts WHERE id= ?;', [req.params.id], (deleteErr, deleteRows, deleteFields) => {
          if (deleteErr) return res.status(400).json({ deleteErr })
          res.status(200).json({ message: 'Les posts ont été effacés.' })
        })
      })
    } else {
      connectData.execute('DELETE FROM Posts WHERE id= ?;', [req.params.id], (deleteErr, deleteRows, deleteFields) => {
        if (deleteErr) return res.status(400).json({ deleteErr })
        res.status(200).json({ message: 'Le post a été effacé.' })
      })
    }
  })
}

// GET ONE POST- READ
exports.getOnePost = (req, res, next) => {
  connectData.execute('SELECT p.*, u.lastname, u.firstname FROM Posts p inner join Users u on p.user_id = u.id;', (err, rows, fields) => {
    if (err) {
      return res.status(400).json({ err })
    }
    res.status(200).json(rows)
  })
}

// GET POST - READ
exports.getAllPosts = (req, res, next) => {
  try {
    // search by the most recent date
    connectData.query('SELECT *, DATE_FORMAT(date, "%d-%m-%Y")AS date FROM posts ORDER BY date DESC LIMIT 20', (err, row) => {
      // if error
      if (err || row.length === 0) {
        return res.status(401).json({ message: 'posts non trouvés' })
      } else {
        const post = row
        // get all posts by date ( most recent in first)
        return res.status(200).json(post)
      }
    })
  } catch (error) {
    return res.status(500).json({ error })
  }
}
