/**
 * Manage some errors with try...catch ;
* get token from header Authorization request(input).
 * verify function(jwt) to decode the Token
 * if not valid=> error
 * get userId of decoded Token ( verified)
 *  **if req.userId? : req.userId === userId
 *  ***if req.userId != userId.decoded => error
 *  *** else user = auth ok
 * execute with function (==> next()) in middleware
*/
// require jwt
const jwt = require('jsonwebtoken')

module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1]
    const verifiedToken = jwt.verify(token, 'RANDOM_TOKEN_SECRET')
    const userId = verifiedToken.userId
    if (req.body.userId && req.body.userId === userId) {
      next()
    } else {
      throw new Error('non')
    }
  } catch (error) {
    res.status(401).json({ error: 'La requête comporte une erreur : ' + error })
  }
}
