/**
 * require multer
 *  create MIME_TYPE to define format file
 * use .diskStorage ( see doc)
 * define a 'model' file as 'nameO2122021.extension'
 * export
  */

const multer = require('multer')

const MIME_TYPES = {
  'image/jpeg': 'jpg',
  'image/jpg': 'jpg',
  'image/png': 'png',
  'images/gif': 'gif'
}

// Total control on  disk storage.
const storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, 'images')
  },
  filename: function (req, file, callback) {
    const name = file.originalname.split(' ').join('_')
    const extension = MIME_TYPES[file.mimetype]
    callback(null, name + Date.now() + '.' + extension)
  }
})

module.exports = multer({ storage: storage }).single('image')
