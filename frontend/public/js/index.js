const token = 'Bearer ' + sessionStorage.getItem('token')

// Create new post
document.getElementById('newPost').addEventListener('submit', async (e) => {
  e.preventDefault()
  const mediaPost = document.getElementById('mediaPost').files[0]
  const post = JSON.stringify({
    userId: sessionStorage.getItem('userId'),
    avatar: sessionStorage.getItem('avatarPost'),
    contentPost: this.contentPost.value
  })

  const formData = new FormData()
  formData.append('image', mediaPost)
  formData.append('post', post)

  const response = await fetch('http://localhost:3006/post/', {
    method: 'POST',
    headers: {
      Authorization: token
    },
    body: formData
  }, true)
  if (response.status === 201) {
    window.location.reload()
  } else {
    alert('Erreur ' + response.status + '. Veuillez réessayer')
  }
})

// Get data from post
const getDataPost = async () => {
  const response = await fetch('http://localhost:3006/post', {
    headers: {
      'Content-Type': 'application/json',
      Authorization: token
    }
  })
  const allPosts = await response.json()
  if (response.status === 200) {
    for (let i = allPosts.length; i > 0; i--) {
      displayAllPosts(allPosts[i - 1])
    }
  } else {
    alert('Erreur ' + response.status + '. Veuillez réessayer')
  }
}
getDataPost()

/**
 * displayallPosts() allows you to build a post. It will be used in a for loop
 * @param {number} post index of a post in an array of allPosts
 */
function displayAllPosts (post) {
  const posts = document.getElementById('allPosts')
  posts.innerHTML +=
    `<div class="card-post">
        <div class="card-post__head">
          <a href="mycount.html?userId=${post.userId}">${post.userName}</a>
          <a onclick="deletePost(${post.id})"><i class="fas fa-trash-alt"></i></a>
        </div>
        <div class="card-post__body">
          <p>${post.contentPost}</p>
          <img class="post__content__img" src="${post.mediaPost}"></img>
        </div>
    </div>`
}

/**
 * deletePost() allows you to delete from the server a post with a specific ID
 * @param {number} postId id of a post
 */
async function deletePost (postId) {
  const response = await fetch('http://localhost:3006/post/' + postId, {
    method: 'DELETE',
    headers: {
      Authorization: token
    }
  })
  if (response.status === 200) {
    window.location.reload()
  } else {
    alert('Erreur ' + response.status + '. Veuillez réessayer')
  }
}
