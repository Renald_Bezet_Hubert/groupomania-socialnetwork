const token = 'Bearer ' + sessionStorage.getItem('token')

// Display user information

const params = new URLSearchParams(document.location.search)

if (params.has('user_id')) {
  const paramValue = params.get('user_id')
  console.log(paramValue, params, 'LOGGING')
  const getUserData = async () => {
    const response = await fetch('http://localhost:3006/user/mycount/' + paramValue, {
      headers: {
        Authorization: token
      }
    })
    const user = await response.json()
    if (response.status === 200) {
      if (user[0].avatar === null) {
        user[0].avatar = '../images/remains/avatar.jpg'
      }
      displayUser(user[0])
    } else {
      alert('Erreur ' + response.status + '. Veuillez réessayer')
    }
  }
  getUserData()
} else {
  alert('Utilisateur introuvable. Vous allez être redirigé')
  window.location = 'index.html'
}

/**
 * displayUser() allows to build a user page with the information of an user
 * user with 4 keys : firstname, lastname, email, avatar
 */
function displayUser (userData) {
  const user = document.getElementById('user')
  user.innerHTML +=
    `<div class="user">
        <div class="user__header">
            <figure class="user__header__figure">
                <img class="user__header__figure__img" src="${userData.avatar}" alt="avatar">
                <button class="user__header__figure__btn" id="btn_update">Modifier</button>
                <button class="user__header__figure__btn user__header__figure__btn--delete" id="btn_delete">Supprimer</button>
            </figure>
            <p class="user__header__name">${userData.firstname} ${userData.lastname}</p>
        </div>
        <div class="user__infos">
            <h2 class="user__infos__title">Infos</h2>
            <div class="user__infos__content">
                <p class="user__infos__content__key">Adresse e-mail</p>
                <p class="user__infos_content__value">${userData.email}</p>
            </div>
        </div>
    </div>`

  document.getElementById('btn_update').addEventListener('click', function (e) {
    e.preventDefault()

    // Display the form to modify user information
    const formUserCount = document.getElementById('formUserCount')
    formUserCount.innerHTML +=
        `<div class="update-user">
            <form id="modifyUserForm">
                <input type="text" id="firstname" name="firstname" placeholder="Votre nouveau prénom">
                <input type="text" id="lastname" name="lastname" placeholder="Votre nouveau nom">
                <input type="file" id="avatar" name="avatar" accept="image/png, image/jpeg, image/jpg">
                <div>
                    <input type="submit" value="Modifier">
                    <button id="update-user-form-cancelled-btn">Annuler</button>
                </div>
            </form>
        </div>`

    // Update user from database
    document.getElementById('modifyUserForm').addEventListener('submit', async (e) => {
      e.preventDefault()
      const avatar = document.getElementById('avatar').files[0]
      const userData = JSON.stringify({
        firstname: document.getElementById('firstname').value,
        lastname: document.getElementById('lastname').value
      })
      const formData = new FormData()
      formData.append('image', avatar)
      formData.append('user', userData)

      const response = await fetch('http://localhost:3006/user/mycount/' + sessionStorage.getItem('user_id'), {
        method: 'PUT',
        headers: {
          Authorization: token
        },
        body: formData
      })
      if (response.status === 200) {
        window.location.reload()
      } else {
        alert('Erreur ' + response.status + '. Veuillez réessayer')
      }
    })

    // Cancel form update user
    document.getElementById('update-user-form-cancelled-btn').addEventListener('click', function () {
      window.location.reload()
    })
  })

  // Delete user from database
  document.getElementById('btn_delete').addEventListener('click', async (e) => {
    e.preventDefault()
    const response = await fetch('http://localhost:3006/user/mycount/' + sessionStorage.getItem('user_id'), {
      method: 'DELETE',
      headers: {
        Authorization: token
      }
    })
    if (response.status === 200) {
      sessionStorage.clear()
      window.location = 'connect.html'
    } else {
      alert('Erreur ' + response.status + '. Veuillez réessayer')
    }
  })
}
