/**
 * element = e
 */

// Create new user
document.getElementById('form-signup').addEventListener('submit', async (e) => {
  e.preventDefault()
  const data = JSON.stringify({
    firstname: this.connect_signup_firstname.value,
    lastname: this.connect_signup_lastname.value,
    email: this.connect_signup_email.value,
    password: this.connect_signup_password.value
  })
  const response = await fetch('http://localhost:3006/user/signup/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: data
  })
  if (response.status === 201) {
    alert('Votre compte est créé. Vous pouvez vous connecter.')
    window.location.reload()
  } else {
    alert('Erreur ' + response.status + '. Veuillez réessayer')
  }
})

// Log in the application
document.getElementById('form-signin').addEventListener('submit', async (e) => {
  e.preventDefault()
  const data = JSON.stringify({
    email: this.connect_signin_email.value,
    password: this.connect_signin_password.value
  })
  const response = await fetch('http://localhost:3006/user/login/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: data
  })
  const dataApi = await response.json()
  if (response.status === 200) {
    sessionStorage.setItem('token', dataApi.token)
    sessionStorage.setItem('userId', dataApi.userId)
    window.location = 'index.html'
  } else {
    alert('Erreur ' + response.status + '. Veuillez réessayer')
  }
})
