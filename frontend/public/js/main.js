document.getElementById('btn-login').addEventListener('click', function (e) {
  const userId = sessionStorage.getItem('userId')
  window.location = 'mycount.html?userId=' + userId
})

document.getElementById('btn-logout').addEventListener('click', function (e) {
  sessionStorage.clear()
  window.location = 'connection.html'
})
