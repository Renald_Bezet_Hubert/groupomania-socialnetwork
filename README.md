# ![logo](./frontend/public/images/logos/icon-left-font.png)

## Présentation de l'application   
Création d'un réseau social d'entreprise pour favoriser les synergies.
### __Une Architecture MVC__ ♺  

#### La partie Front-end 💻
La partie View(v) a été développée à part. Nous reviendrons sur ses caractéristiques dans la partie __Installation__ 🛠
#### La partie Back-end
##### *L'API*
Elle permettra de faire le lien avec la Base de données(Mysql)
### Les fonctionnalités de l'application:
- S'inscrire et/ se connecter (signup et login)
- consulter les posts ajoutées par les utilisateurs
- Ajouter des posts
- Modifier ou supprimer son compte.
- Modifier son post
- Droits administrateur spécifiques

### **La sécurité** :
- l’API doit respecter le RGPD et les standards OWASP
- le mot de passe des utilisateurs doit être chiffré
- 2 types de droits administrateur à la base de données doivent être définis:
    - un accès pour supprimer ou modifier des tables
    - et un accès pour éditer le contenu de la base de données 
- l’authentification est renforcée sur les routes requises
- les mots de passe sont stockés de manière sécurisée ;
- les adresses mails de la base de données sont uniques et un plugin approprié est utilisé pour s’assurer de leur caractère unique et rapporter des erreurs.

##Installation 🛠

### Stack:⚙️

- Backend: NodeJs / Express
- Frontend: Sass
- Base De Donnée: alwaysdata
- Service: Mysql  

### Prérequis:

- Installer NodeJs et MySql.
- Cloner le projet sur votre machine
- Copier le contenu du fichier .env fourni à part dans le fichier du même nom situé dans le projet (backend > .env).

### Importation de la base de données:

- Dans un serveur MySql, entrez la commande suivante pour créer une nouvelle base de donnéees :
  #### `CREATE DATABASE groupomania;`
  
- Dans le fichier .env, remplacer [your_root_password] par votre mot de passe root mysql.  

- Pour importer le contenu du fichier groupomania.sql (backend > sql > database.sql) dans cette nouvelle base de données, entrez la commande suivante, 
  en remplaçant "user" et "password" par les données du fichier .env :

  #### `mysql -u user -p password groupomania < groupomania.sql`

## Lancement de l'application :

- Allez dans le dossier backend de l'application :

  - installez les dépendances avec la commande suivante :
    #### `npm install`
  - lancez le serveur avec la commande suivante :
    #### `nodemon`
    
- Allez dans le dossier frontend de l'application :

  - installez les dépendances avec la commande suivante :
    #### `npm install`
  - lancez l'application avec la commande suivante :
    #### `npm start` (http://localhost:3006/)
